// The Swift Programming Language
// https://docs.swift.org/swift-book

public class Calculator {
    public func Add(a: Int, b: Int) -> Int {
        return a + b
    }
}
